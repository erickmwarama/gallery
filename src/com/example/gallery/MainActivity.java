package com.example.gallery;

import android.app.Activity;
import android.os.*;
import android.view.*;
import android.widget.*;
import android.content.*;

public class MainActivity extends Activity {

	public Integer[] images=new Integer[]{R.drawable.one,R.drawable.two,R.drawable.three,R.drawable.four};
	@SuppressWarnings("deprecation")
	private Gallery mygallery;
	private ImageView myimage;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        mygallery=(Gallery)findViewById(R.id.gallery1);
        myimage=(ImageView)findViewById(R.id.imageview);
        
        mygallery.setAdapter(new MyAdapter(MainActivity.this,images));
       mygallery.setOnItemClickListener(new AdapterView.OnItemClickListener()
       {
    	   public void onItemClick(AdapterView<?>parent,View v,int pos,long id)
    	   {
    		   if(pos > 3)
    		   {
    			   pos=(pos % 4);
    		   }
    		   myimage.setImageResource(images[pos]);
    	   }
       });
    }
}

 class MyAdapter extends BaseAdapter{
	private Integer[]imagesarray;
	private Context con;
	public MyAdapter(Context c,Integer[]a)
	{
		this.imagesarray=a;
		this.con=c;
	}
	public int getCount()
	{
		//return imagesarray.length+1;
		return 100;
	}
	
	public Object getItem(int pos)
	{
		return pos;
	}
	
	public long getItemId(int id)
	{
		return id;
	}
	
	public View getView(int pos,View v,ViewGroup vg)
	{
		ImageView iv=new ImageView(con);
		if(pos>3)
		{
			pos=(pos % 4);
		}
		iv.setImageResource(imagesarray[pos]);
		
		return iv;
	}
}
